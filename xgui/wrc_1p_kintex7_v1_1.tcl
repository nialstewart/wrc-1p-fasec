# Definitional proc to organize widgets for parameters.
proc init_gui { IPINST } {
  ipgui::add_param $IPINST -name "Component_Name"
  #Adding Page
  set Page_0 [ipgui::add_page $IPINST -name "Page 0"]
  ipgui::add_param $IPINST -name "TAR_ADDR_WDTH" -parent ${Page_0}


}

proc update_PARAM_VALUE.TAR_ADDR_WDTH { PARAM_VALUE.TAR_ADDR_WDTH } {
	# Procedure called to update TAR_ADDR_WDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.TAR_ADDR_WDTH { PARAM_VALUE.TAR_ADDR_WDTH } {
	# Procedure called to validate TAR_ADDR_WDTH
	return true
}


proc update_MODELPARAM_VALUE.TAR_ADDR_WDTH { MODELPARAM_VALUE.TAR_ADDR_WDTH PARAM_VALUE.TAR_ADDR_WDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.TAR_ADDR_WDTH}] ${MODELPARAM_VALUE.TAR_ADDR_WDTH}
}

