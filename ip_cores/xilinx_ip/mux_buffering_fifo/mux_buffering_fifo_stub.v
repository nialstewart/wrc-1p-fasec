// Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2016.2 (lin64) Build 1577090 Thu Jun  2 16:32:35 MDT 2016
// Date        : Tue Mar 14 17:29:28 2017
// Host        : lapte24154 running 64-bit openSUSE Leap 42.1 (x86_64)
// Command     : write_verilog -force -mode synth_stub
//               /home/pieter/Development/projects/FIDS/sevensols/wrc-2p/ip_cores/xilinx_ip/mux_buffering_fifo/mux_buffering_fifo_stub.v
// Design      : mux_buffering_fifo
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z030ffg676-2
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "fifo_generator_v13_1_1,Vivado 2016.2" *)
module mux_buffering_fifo(clk, srst, din, wr_en, rd_en, prog_full_thresh_assert, prog_full_thresh_negate, dout, full, almost_full, empty, valid, prog_full)
/* synthesis syn_black_box black_box_pad_pin="clk,srst,din[17:0],wr_en,rd_en,prog_full_thresh_assert[4:0],prog_full_thresh_negate[4:0],dout[17:0],full,almost_full,empty,valid,prog_full" */;
  input clk;
  input srst;
  input [17:0]din;
  input wr_en;
  input rd_en;
  input [4:0]prog_full_thresh_assert;
  input [4:0]prog_full_thresh_negate;
  output [17:0]dout;
  output full;
  output almost_full;
  output empty;
  output valid;
  output prog_full;
endmodule
